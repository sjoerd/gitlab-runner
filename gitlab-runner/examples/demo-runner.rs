//use bytes::BufMut;
//use bytes::Bytes;
use gitlab_runner::job::Job;
use gitlab_runner::{JobHandler, JobResult, Phase, Runner};
use log::info;
use serde::Deserialize;
//use std::io::Write;
use structopt::StructOpt;
use url::Url;

#[derive(StructOpt)]
struct Opts {
    server: Url,
    token: String,
}

#[derive(Deserialize)]
struct Fact {
    fact: String,
}

#[derive(Deserialize)]
struct Reply {
    data: Vec<Fact>,
}

/*
async fn do_artifacts(j: &Job, amount: u8) -> Result<(), ()> {
    let facts = get_facts(amount).await?;

    let mut buf = Vec::new();
    let mut zip = zip::ZipWriter::new(std::io::Cursor::new(&mut buf));

    let options =
        zip::write::FileOptions::default().compression_method(zip::CompressionMethod::Stored);

    for (i, f) in facts.iter().enumerate() {
        zip.start_file(format!("fact{}.txt", i), options).unwrap();
        zip.write(f.as_bytes()).unwrap();
    }

    // Apply the changes you've made.
    // Dropping the `ZipWriter` will have the same effect, but may silently fail
    zip.finish().unwrap();
    drop(zip);

    j.upload_artifact(buf).await.unwrap();
    Ok(())
}
*/

/*
struct Content {
    name: String,
    data: Bytes,
}

fn dump_zip(bytes: Bytes) -> Vec<Content> {
    let reader = std::io::Cursor::new(bytes);
    let mut zip = zip::ZipArchive::new(reader).unwrap();
    let mut contents = Vec::with_capacity(zip.len());
    for i in 0..zip.len() {
        let mut f = zip.by_index(i).unwrap();
        let name = f.name().to_string();
        let mut buf = bytes::BytesMut::with_capacity(f.size() as usize).writer();
        std::io::copy(&mut f, &mut buf).unwrap();
        contents.push(Content {
            name,
            data: buf.into_inner().into(),
        });
    }
    contents
}
*/

struct Run {
    client: reqwest::Client,
    amount: u8,
    job: Job,
}

impl Run {
    fn new(job: Job, amount: u8) -> Self {
        let client = reqwest::Client::new();
        Run {
            client,
            amount,
            job,
        }
    }

    async fn get_facts(&self, amount: u8) -> Result<Vec<String>, ()> {
        let mut url = Url::parse("https://catfact.ninja/facts").unwrap();
        url.query_pairs_mut()
            .append_pair("limit", &format!("{}", amount));

        let mut r: Reply = self
            .client
            .get(url)
            .send()
            .await
            .map_err(|_| ())?
            .json()
            .await
            .map_err(|_| ())?;
        Ok(r.data.drain(..).map(|f| f.fact).collect())
    }

    async fn command(&self, command: &str) -> JobResult {
        self.job.trace(format!("> {}\n", command)).await;
        let mut p = command.split_whitespace();
        if let Some(cmd) = p.next() {
            if cmd != "fact" {
                self.job.trace("Unknown command").await;
                return Err(());
            }

            let amount = match p.next() {
                Some(amount) => match amount.parse() {
                    Ok(a) => a,
                    Err(e) => {
                        self.job
                            .trace(format!("Failed to parse amount: {}", e))
                            .await;
                        return Err(());
                    }
                },
                _ => self.amount,
            };

            let facts = self.get_facts(amount).await?;
            for f in facts {
                self.job.trace(format!("Did you know: {}\n", f)).await;
            }
            Ok(())
        } else {
            self.job.trace("empty command").await;
            Err(())
        }
    }
}

#[async_trait::async_trait]
impl JobHandler for Run {
    async fn step(&mut self, script: &[String], _phase: Phase) -> JobResult {
        for command in script {
            self.command(command).await?;
        }

        Ok(())
    }
}

#[tokio::main]
async fn main() {
    env_logger::init();
    let opts = Opts::from_args();
    let mut runner = Runner::new(opts.server, opts.token);
    runner
        .run(
            move |job| async move {
                let amount = match job.variable("AMOUNT") {
                    Some(v) => v.value().parse().expect("Amount not a number"),
                    _ => 1u8,
                };

                info!("Created run");
                Ok(Run::new(job, amount))

                /*
                for d in job.dependencies() {
                    job.log(format!("Artifacts from : {}\n", d.name()))
                        .await
                        .unwrap();
                    let download = d.download().await.unwrap();
                    if let Some(data) = download {
                        job.log(format!("Artifacts from : {}\n", d.name()))
                            .await
                            .unwrap();
                        for c in dump_zip(data) {
                            job.log(format!("Name: {}\n", c.name)).await.unwrap();
                            let data = String::from_utf8_lossy(&*c.data);
                            job.log(format!("data:\n{}\n", data)).await.unwrap();
                        }
                    }
                }

                if let Some(script) = job.script() {
                    for c in script.script() {
                        handle_command(&job, c, amount).await?;
                    }
                }

                if job.artifact().is_some() {
                    do_artifacts(&job, amount).await?;
                }

                Ok(Run {})
                    */
            },
            8,
        )
        .await
        .expect("Couldn't pick up jobs");
}
