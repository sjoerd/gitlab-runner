use futures::AsyncWriteExt;
use gitlab_runner::job::Job;
use gitlab_runner::uploader::Uploader;
use gitlab_runner::{JobHandler, JobResult, Phase, Runner};
use gitlab_runner_mock::{GitlabRunnerMock, MockJobState, MockJobStepName, MockJobWhen};

struct Upload();

#[async_trait::async_trait]
impl JobHandler for Upload {
    async fn step(&mut self, _script: &[String], _phase: Phase) -> JobResult {
        Ok(())
    }

    async fn upload_artifacts(&mut self, uploader: &mut Uploader) -> JobResult {
        let mut f = uploader.file("test".to_string()).await;
        f.write_all(b"testdata")
            .await
            .expect("Couldn't write test data");
        Ok(())
    }
}

struct Download();

impl Download {
    async fn new(job: Job) -> Self {
        assert_eq!(job.dependencies().count(), 1);
        for d in job.dependencies() {
            eprintln!("=> {:?}", d.download().await);
        }
        Download()
    }
}

#[async_trait::async_trait]
impl JobHandler for Download {
    async fn step(&mut self, _script: &[String], _phase: Phase) -> JobResult {
        Ok(())
    }
}

#[tokio::test]
async fn artifacts() {
    let mock = GitlabRunnerMock::start().await;
    let upload = mock.add_dummy_job("upload artifact".to_string());

    let mut download = mock.job_builder("download artifact".to_string());
    download.dependency(upload.clone());
    download.add_step(
        MockJobStepName::Script,
        vec!["dummy".to_string()],
        3600,
        MockJobWhen::OnSuccess,
        false,
    );

    let download = download.build();

    mock.enqueue_job(download.clone());

    let mut runner = Runner::new(mock.uri(), mock.runner_token().to_string());

    // Upload job comes first
    let got_job = runner
        .request_job(|_job| async move { Ok(Upload()) })
        .await
        .unwrap();
    assert_eq!(true, got_job);
    runner.wait_job().await;
    assert_eq!(MockJobState::Success, upload.state());

    let got_job = runner
        .request_job(|job| async move { Ok(Download::new(job).await) })
        .await
        .unwrap();
    assert_eq!(true, got_job);
    runner.wait_job().await;
    assert_eq!(MockJobState::Success, upload.state());

    /*
    assert_eq!(DATA, upload.artifact());

    let got_job = runner
        .request_job(|job| async move {
            let dependencies = job.dependencies();
            assert_eq!(1, dependencies.len());
            let d = &dependencies[0];
            let data = d.download().await.unwrap().unwrap();
            assert_eq!(DATA, data);
            SimpleRun::dummy(Ok(())).await
        })
        .await
        .unwrap();
    assert_eq!(true, got_job);
    runner.wait_job().await;
    assert_eq!(MockJobState::Success, upload.state());
    */
}
